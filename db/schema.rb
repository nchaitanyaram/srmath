# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110402061728) do

  create_table "gotrams", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seva_bookings", :force => true do |t|
    t.string   "devotee_name",   :null => false
    t.integer  "gotram_id",      :null => false
    t.string   "gotram_name"
    t.integer  "seva_id",        :null => false
    t.string   "seva_name"
    t.date     "seva_date"
    t.date     "booking_date"
    t.integer  "amount",         :null => false
    t.integer  "no_of_persons"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.string   "reciept_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sevas", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.float    "rate"
    t.integer  "max_persons"
    t.string   "short_code"
    t.integer  "seva_number", :default => 0
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
