class CreateGotrams < ActiveRecord::Migration
  def self.up
    create_table :gotrams do |t|
      t.string :name
      t.text :description
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :gotrams
  end
end
