class CreateSevaBookings < ActiveRecord::Migration
  def self.up
    create_table :seva_bookings do |t|
      t.string :devotee_name, :null=>false
      t.integer :gotram_id, :null=>false
      t.string  :gotram_name
      t.integer :seva_id, :null=>false
      t.string :seva_name
      t.date :seva_date
      t.date :booking_date
      t.integer :amount, :null=>false
      t.integer :no_of_persons

      t.integer :created_by
      t.integer :modified_by
      t.string :reciept_number

      t.timestamps
    end
  end

  def self.down
    drop_table :seva_bookings
  end
end

