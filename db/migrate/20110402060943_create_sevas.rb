class CreateSevas < ActiveRecord::Migration
  def self.up
    create_table :sevas do |t|
      t.string :name
      t.text :description
      t.float :rate
      t.integer :max_persons
      t.string  :short_code
      t.integer :seva_number, :default=>0
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :sevas
  end
end

