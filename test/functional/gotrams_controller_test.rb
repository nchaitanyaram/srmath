require 'test_helper'

class GotramsControllerTest < ActionController::TestCase
  setup do
    @gotram = gotrams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gotrams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gotram" do
    assert_difference('Gotram.count') do
      post :create, :gotram => @gotram.attributes
    end

    assert_redirected_to gotram_path(assigns(:gotram))
  end

  test "should show gotram" do
    get :show, :id => @gotram.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @gotram.to_param
    assert_response :success
  end

  test "should update gotram" do
    put :update, :id => @gotram.to_param, :gotram => @gotram.attributes
    assert_redirected_to gotram_path(assigns(:gotram))
  end

  test "should destroy gotram" do
    assert_difference('Gotram.count', -1) do
      delete :destroy, :id => @gotram.to_param
    end

    assert_redirected_to gotrams_path
  end
end
