require 'test_helper'

class SevaBookingsControllerTest < ActionController::TestCase
  setup do
    @seva_booking = seva_bookings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:seva_bookings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create seva_booking" do
    assert_difference('SevaBooking.count') do
      post :create, :seva_booking => @seva_booking.attributes
    end

    assert_redirected_to seva_booking_path(assigns(:seva_booking))
  end

  test "should show seva_booking" do
    get :show, :id => @seva_booking.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @seva_booking.to_param
    assert_response :success
  end

  test "should update seva_booking" do
    put :update, :id => @seva_booking.to_param, :seva_booking => @seva_booking.attributes
    assert_redirected_to seva_booking_path(assigns(:seva_booking))
  end

  test "should destroy seva_booking" do
    assert_difference('SevaBooking.count', -1) do
      delete :destroy, :id => @seva_booking.to_param
    end

    assert_redirected_to seva_bookings_path
  end
end
