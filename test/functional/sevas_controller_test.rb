require 'test_helper'

class SevasControllerTest < ActionController::TestCase
  setup do
    @seva = sevas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sevas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create seva" do
    assert_difference('Seva.count') do
      post :create, :seva => @seva.attributes
    end

    assert_redirected_to seva_path(assigns(:seva))
  end

  test "should show seva" do
    get :show, :id => @seva.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @seva.to_param
    assert_response :success
  end

  test "should update seva" do
    put :update, :id => @seva.to_param, :seva => @seva.attributes
    assert_redirected_to seva_path(assigns(:seva))
  end

  test "should destroy seva" do
    assert_difference('Seva.count', -1) do
      delete :destroy, :id => @seva.to_param
    end

    assert_redirected_to sevas_path
  end
end
