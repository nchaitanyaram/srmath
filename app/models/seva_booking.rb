class SevaBooking < ActiveRecord::Base
  belongs_to :gotram
  belongs_to :seva
  validates_presence_of :devotee_name
end

