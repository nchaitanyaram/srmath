class Gotram < ActiveRecord::Base
    has_many :seva_bookings
    validates_uniqueness_of :name
end

