class GotramsController < ApplicationController
  # GET /gotrams
  # GET /gotrams.xml
  def index
    @gotrams = Gotram.order("name ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @gotrams }
    end
  end

  # GET /gotrams/1
  # GET /gotrams/1.xml
  def show
    @gotram = Gotram.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @gotram }
    end
  end

  # GET /gotrams/new
  # GET /gotrams/new.xml
  def new
    @gotram = Gotram.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @gotram }
    end
  end

  # GET /gotrams/1/edit
  def edit
    @gotram = Gotram.find(params[:id])
  end

  # POST /gotrams
  # POST /gotrams.xml
  def create
    @gotram = Gotram.new(params[:gotram])

    respond_to do |format|
      if @gotram.save
        format.html { redirect_to(@gotram, :notice => 'Gotram was successfully created.') }
        format.xml  { render :xml => @gotram, :status => :created, :location => @gotram }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @gotram.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /gotrams/1
  # PUT /gotrams/1.xml
  def update
    @gotram = Gotram.find(params[:id])

    respond_to do |format|
      if @gotram.update_attributes(params[:gotram])
        format.html { redirect_to(@gotram, :notice => 'Gotram was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @gotram.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /gotrams/1
  # DELETE /gotrams/1.xml
  def destroy
    @gotram = Gotram.find(params[:id])
    @gotram.destroy

    respond_to do |format|
      format.html { redirect_to(gotrams_url) }
      format.xml  { head :ok }
    end
  end
end
