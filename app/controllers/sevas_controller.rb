class SevasController < ApplicationController

  def index
    @sevas = Seva.order("name ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @sevas }
    end
  end

  def show
    @seva = Seva.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @seva }
    end
  end

  def new
    @seva = Seva.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @seva }
    end
  end

  def edit
    @seva = Seva.find(params[:id])
  end

  def create
    @seva = Seva.new(params[:seva])
   
    @seva.short_code = params[:seva][:short_code].upcase
    respond_to do |format|
      if @seva.save
        format.html { redirect_to(@seva, :notice => 'Seva was successfully created.') }
        format.xml  { render :xml => @seva, :status => :created, :location => @seva }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @seva.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @seva = Seva.find(params[:id])

    respond_to do |format|
      if @seva.update_attributes(params[:seva])
        format.html { redirect_to(@seva, :notice => 'Seva was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @seva.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @seva = Seva.find(params[:id])
    @seva.destroy

    respond_to do |format|
      format.html { redirect_to(sevas_url) }
      format.xml  { head :ok }
    end
  end

  def sevareport
    @sevas=Seva.order("name ASC")
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css' 
    send_data(kit.to_pdf, :filename => "Seva.pdf", :type => 'application/pdf')
  end

end

