class SevaBookingsController < ApplicationController
  layout 'application', :except => :print
  def index
    @seva_bookings = SevaBooking.order("reciept_number ASC, booking_date DESC")
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @seva_bookings }
    end
  end
  
  def show
    @seva_booking = SevaBooking.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      
    end
  end
  
  
  def print
    @seva_booking = SevaBooking.find(params[:id])
=begin
    #PDF Render   
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
#    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/screen.css' 
    send_data(kit.to_pdf, :filename => "Reciept-#{params[:id]}.pdf", :type => 'application/pdf')
=end
  end
  
  def new
    @seva_booking = SevaBooking.new
    @gotrams = Gotram.order("name ASC")
    @sevas = Seva.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @seva_booking }
    end
  end
  
  def edit
    @seva_booking = SevaBooking.find(params[:id])
    @gotrams = Gotram.all
    @sevas = Seva.all
    
  end
  
  def create
    @seva_booking = SevaBooking.new(params[:seva_booking])
    #Increment seva_number in sevas
    seva =Seva.find(params[:seva_booking][:seva_id])
    seva.increment!(:seva_number)
    @seva_booking.reciept_number = seva.short_code + "%05d" % seva.seva_number
    
    if @seva_booking.seva.name="Others"
      @seva_booking.amount = params[:seva_booking][:amount]
      #      puts"########################"
      @seva_booking.amount
    else
      @seva_booking.amount = @seva_booking.seva.rate
      #      puts"########################"
      @seva_booking.amount
    end
    
    respond_to do |format|
      if @seva_booking.save
        format.html { redirect_to(@seva_booking, :notice => 'Seva Booking was successfully created.') }
        format.xml  { render :xml => @seva_booking, :status => :created, :location => @seva_booking }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @seva_booking.errors, :status => :unprocessable_entity }
      end
    end
    
  end
  
  def update
    @seva_booking = SevaBooking.find(params[:id])
    @seva_booking.amount = params[:seva_booking]? params[:seva_booking]:@seva_booking.seva.rate
    respond_to do |format|
      if @seva_booking.update_attributes(params[:seva_booking])
        format.html { redirect_to(@seva_booking, :notice => 'Seva Booking was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @seva_booking.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @seva_booking = SevaBooking.find(params[:id])
    @seva_booking.destroy
    
    respond_to do |format|
      format.html { redirect_to(seva_bookings_url) }
      format.xml  { head :ok }
    end
  end
  
  def eodreport
    @sevas = Seva.includes([:seva_bookings]).where("seva_bookings.booking_date=?",Date.today).order(:name)
    respond_to do |format|
      format.html
    end
    
    
  end
  
  def eodreport_print
    # @eodreport = SevaBooking.where(:booking_date => Date.today)
    @sevas = Seva.includes([:seva_bookings]).where("seva_bookings.booking_date=?",Date.today).order(:name)
    #PDF Render   
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css' 
    send_data(kit.to_pdf, :filename => "labels.pdf", :type => 'application/pdf')
    
  end
  
  def date_range_report
    if params[:start_date] == nil && params[:end_date] == nil
      @rangereport = []
    else
      @rangereport = SevaBooking.where(:seva_date => params[:start_date].to_date..params[:end_date].to_date)
    end
    
    respond_to do |format|
      format.html
    end
  end
  
  def date_range_report_print   
    if params[:start_date] == nil && params[:end_date] == nil
      @sevas = []
    else
      @sevas = Seva.includes([:seva_bookings]).where("seva_bookings.seva_date>=? AND seva_bookings.seva_date<=?" ,params[:start_date].to_date,params[:end_date].to_date).order(:name)
    end
    #      @sevas = Seva.includes([:seva_bookings]).where("seva_bookings.seva_date>=? AND seva_bookings.seva_date<=?" ,params[:start_date].to_date,params[:end_date].to_date).order(:name)
    
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css' 
    send_data(kit.to_pdf, :filename => "Seva_Report.pdf", :type => 'application/pdf')
    
    
  end
  
  def seva_report
    #    @sevareport = SevaBooking.find(:all, :conditions=>{:seva_date => Date.today})
    if params[:start_date] == nil && params[:end_date] == nil
      @rangereport = []
    else
      @rangereport = SevaBooking.where(:booking_date => params[:start_date].to_date..params[:end_date].to_date)
    end
    respond_to do |format|
      format.html
     
    end
  end
  
  def seva_report_print
    @sevas = Seva.includes([:seva_bookings]).where("seva_bookings.booking_date" => params[:start_date].to_date..params[:end_date].to_date).order("name")
    
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css' 
    send_data(kit.to_pdf, :filename => "Date_Range_Report.pdf", :type => 'application/pdf')
  end
  
  def get_persons
    @seva = Seva.find(params[:id])
    
    respond_to do |format|
      format.js
    end
  end
  
  def save_reciept
    @seva_booking = SevaBooking.find(params[:seva_booking_id])
    @seva_booking.reciept_number = params[:reciept_number]
    @seva_booking.save
    respond_to do |format|
      format.js
    end
    
  end
  
  def grt
    @seva_booking = SevaBooking.new
    @gotrams = Gotram.order("name ASC")
    @sevas = Seva.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @seva_booking }
    end
  end
  
  def othereodreport
    @eodreport = SevaBooking.find(:all,:conditions=>{:booking_date=>Date.today, :seva_id=>36},:order => "reciept_number DESC")  
  end
  def othereodreport_print
    @eodreport = SevaBooking.find(:all,:conditions=>{:booking_date=>Date.today, :seva_id=>36},:order => "reciept_number ASC") 
    html = render_to_string :layout => false 
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css' 
    send_data(kit.to_pdf, :filename => "OtherEDDReport.pdf", :type => 'application/pdf')
  end
end
